import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { retry } from 'rxjs/operators';
import { MidlayerService } from 'src/app/thirdparty/midlayer.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';



@Injectable({
  providedIn: 'root'
})
export class ClientcallService {
  apiurl = '';
  constructor(private httpClient: HttpClient, private midlay: MidlayerService, private cookieService: CookieService,
    private router: Router,
    private spinner: NgxSpinnerService,


    ) 
  {this.apiurl = midlay.globalsetting.baseurl ;}
   
  public clinetposturlopen(req: any,apiname:any) { 
       let inputreq=this.userauthnticate(req);
       let encobj=this.midlay.enccall(JSON.stringify(inputreq));const reqobj={ _Clients3a2:encobj}; 
       const result:any = this.httpClient .post(`${this.apiurl}${apiname}`, reqobj, this.midlay.getPostHttpOptionsplain(encobj,encobj)).pipe(retry(this.midlay.globalsetting.retry)).toPromise();
       return result;
    }


 /*  public clinetposturlauth(req: any,apiname:any) {
    let inputreq=this.userauthnticate(req);
    let encobj=this.midlay.enccall(JSON.stringify(inputreq));const reqobj={ _Clients3a2:encobj}; 
    const result: any = this.httpClient.post(`${this.apiurl}${apiname}`,encobj,this.midlay.getPostHttpOptionauth(encobj,encobj) ).pipe(retry(this.midlay.globalsetting.retry)).toPromise();
    return result;;
  } */
  public clinetposturlauth(req: any,apiname:any) {
    // let inputreq=this.userauthnticate(req); 
     let encobj=this.midlay.enccall(JSON.stringify(req));
     const reqobj={ _Clients3a2:encobj}; 
     const result: any = this.httpClient.post(`${this.apiurl}${apiname}`,reqobj,this.midlay.getPostHttpOptionauth(encobj,encobj) ).pipe(retry(this.midlay.globalsetting.retry)).toPromise();
     return result;;
 
 
     // let encobj = this.midlay.enccall(JSON.stringify(req));
     // const reqobj = { _Clients3a2: encobj };
     // const result: any = this.httpClient.post(`${this.apiurl}${apiname}`, reqobj, this.midlay.HttpOptions(hetoken, refid, encobj, encobj)).pipe(retry(this.midlay.globalsetting.retry)).toPromise();
     // return result;
   }

  public clinetposturlauth_withoutenc(req: any,apiname:any) { 
    // const inputreq=this.userauthnticate(req);
    // let reqobj=JSON.stringify(inputreq)
    // //let encobj={_Clients3a2:this.midlay.enccall(inputreq)};
    // const result: any = this.httpClient.post(`${this.apiurl}${apiname}`,inputreq,this.midlay.HttpOptions() ).pipe(retry(this.midlay.globalsetting.retry)).toPromise();
    // return result;;


   // let encobj={_Clients3a2:this.midlay.enccall(req)};
    const result: any = this.httpClient.post(`${this.apiurl}${apiname}`,req,this.midlay.HttpOptions() ).pipe(retry(this.midlay.globalsetting.retry)).toPromise();
    return result;;
  }

  public clinetposturlauth_withoutenc_Login(req: any,apiname:any) {
    let encobj={_Clients3a2:this.midlay.enccall(req)};
    const result: any = this.httpClient.post(`${this.apiurl}${apiname}`,req,this.midlay.HttpOptions() ).pipe(retry(this.midlay.globalsetting.retry)).toPromise();
    return result;;
  }
  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }
  userauthnticate(req:any){
    let obj: any = this.midlay.Getuser();
    if(obj!=""&&obj!=undefined&&obj!=null){
      req.usersno= obj[0].EMP_ID;
      req.roleid= obj[0].ROLE_ID;
      req.source="web";
      req.requestip=this.cookieService.get('Deviceid');
      req.Browser=this.detectBrowserVersion();
      req.hskvalue=this.cookieService.get('_Uid');
      req.latitude="";
      req.longitude="";
      req.gioaddress="";
      req.refno=this.cookieService.get('_cltkn');
      return req;
    }
    else{
      this.router.navigate(['/Sessionexpired']);
    }
    
   
  }   
  detectBrowserName() { 
    const agent = window.navigator.userAgent.toLowerCase()
    switch (true) {
      case agent.indexOf('edge') > -1:
        return 'edge';
      case agent.indexOf('opr') > -1 && !!(<any>window).opr:
        return 'opera';
      case agent.indexOf('chrome') > -1 && !!(<any>window).chrome:
        return 'chrome';
      case agent.indexOf('trident') > -1:
        return 'ie';
      case agent.indexOf('firefox') > -1:
        return 'firefox';
      case agent.indexOf('safari') > -1:
        return 'safari';
      default:
        return 'other';
    }
  }
  // getIpCliente() {
  //   return this.httpClient
  //              .get('http://api.ipify.org/?format=jsonp&callback=JSONP_CALLBACK')
  //              .map((res: Response) => {
  //                console.log('res ', res);
  //                console.log('res.json() ', res.text());
  //                console.log('parseado  stringify ', JSON.stringify(res.text()));
  //                let ipVar = res.text();
  //                let num = ipVar.indexOf(":");
  //                let num2 = ipVar.indexOf("\"});");
  //                ipVar = ipVar.slice(num+2,num2);
  
  //                return ipVar
  //              }
  //   );
  // }
  
   
  detectBrowserVersion(){
      var userAgent = navigator.userAgent, tem, 
      matchTest = userAgent.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
      
      if(/trident/i.test(matchTest[1])){
          tem =  /\brv[ :]+(\d+)/g.exec(userAgent) || [];
          return 'IE '+(tem[1] || '');
      }
      if(matchTest[1]=== 'Chrome'){
          tem = userAgent.match(/\b(OPR|Edge)\/(\d+)/);
          if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
      }
      matchTest= matchTest[2]? [matchTest[1], matchTest[2]]: [navigator.appName, navigator.appVersion, '-?'];
      if((tem= userAgent.match(/version\/(\d+)/i))!= null) matchTest.splice(1, 1, tem[1]);
      return matchTest.join(' ');
  }
  
}
