import { Component } from '@angular/core';

interface TableColumn {
  name: string;
  prop: string;
  selected: boolean;
}
@Component({
  selector: 'app-dynamictable',
  templateUrl: './dynamictable.component.html',
  styleUrls: ['./dynamictable.component.css']
})
export class DynamictableComponent {
  columns: TableColumn[] = [
    { name: 'Column 1', prop: 'col1', selected: true },
    { name: 'Column 2', prop: 'col2', selected: true },
    { name: 'Column 3', prop: 'col3', selected: true },
    { name: 'Column 4', prop: 'col4', selected: true },
  ];

  data = [
    { col1: 'Row 1, Column 1', col2: 'Row 1, Column 2', col3: 'Row 1, Column 3', col4: 'Row 1, Column 4' },
    { col1: 'Row 2, Column 1', col2: 'Row 2, Column 2', col3: 'Row 2, Column 3', col4: 'Row 2, Column 4' },
    { col1: 'Row 3, Column 1', col2: 'Row 3, Column 2', col3: 'Row 3, Column 3', col4: 'Row 3, Column 4' },
  ];

  displayedColumns: TableColumn[] = this.columns.filter(column => column.selected);

  toggleColumn(column: TableColumn) {
    column.selected = !column.selected;
    this.displayedColumns = this.columns.filter(column => column.selected);
  }
}
