import { NgModule,NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { DeviceDetectorService } from 'ngx-device-detector';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ToastrModule } from 'ngx-toastr';
import { environment } from '../environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BadrequestComponent } from './security/badrequest/badrequest.component';
import { LogoutComponent } from './security/logout/logout.component';
import { SessionexpiredComponent } from './security/sessionexpired/sessionexpired.component';
import { UnauthorizedaccessComponent } from './security/unauthorizedaccess/unauthorizedaccess.component';
import { NgApexchartsModule } from 'ng-apexcharts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { QRCodeModule } from 'angularx-qrcode';
import { GoogleMapsModule } from '@angular/google-maps'
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MenuheaderComponent } from './thirdparty/menuheader/menuheader.component';
import { MenufooterComponent } from './thirdparty/menufooter/menufooter.component';
import { DataTablesModule } from 'angular-datatables';
import { SignalRServiceService } from './thirdparty/signal-rservice.service'; 
import { ReactiveFormsModule } from '@angular/forms';

// Import the library
import { CarouselModule } from 'ngx-owl-carousel-o';
import { OwlOptions } from 'ngx-owl-carousel-o';// Needs to import the BrowserAnimationsModule
import { ExportAsModule } from 'ngx-export-as';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { NgFor } from '@angular/common';





import { AdminLayoutComponent } from './Admin/Layout/admin-layout/admin-layout.component';
import {LayoutSidemenuComponent} from './Admin/Layout/layout-sidemenu/layout-sidemenu.component';
import {MasterHeaderComponent} from './Admin/Layout/master-header/master-header.component';
import { SacSurveyDashboardComponent } from './Admin/Modules/sac-survey-dashboard/sac-survey-dashboard.component';
import { SacSurveyReportComponent } from './Admin/Modules/sac-survey-report/sac-survey-report.component';
import { LoginComponent } from './Admin/login/login.component';
import { DashboardComponent } from './Admin/Modules/dashboard/dashboard.component';
import { ChangepasswordComponent } from './Admin/Modules/changepassword/changepassword.component';
import { GenderReportComponent } from './Admin/Modules/Deptreports/gender-report/gender-report.component';
import { CasteReportComponent } from './Admin/Modules/Deptreports/caste-report/caste-report.component';
import { AgeReportComponent } from './Admin/Modules/Deptreports/age-report/age-report.component';
import { HealthReportComponent } from './Admin/Modules/Deptreports/health-report/health-report.component';
import { FemaleheadfamiliesComponent } from './Admin/Modules/Deptreports/femaleheadfamilies/femaleheadfamilies.component';
import { EmploymentReportComponent } from './Admin/Modules/Deptreports/employment-report/employment-report.component';
import { EducationalReportComponent } from './Admin/Modules/Deptreports/educational-report/educational-report.component';
import { NewhomeComponent } from './Admin/Modules/newhome/newhome.component';

import { StateHomeComponent } from './Admin/Modules/state-home/state-home/state-home.component';
import { WorkActivityReportComponent } from './Admin/Modules/Deptreports/work-activity-report/work-activity-report/work-activity-report.component';
import { BarchartComponent } from './Admin/Modules/barchart/barchart.component';
import { SacsurveycheckerformComponent } from './Admin/Modules/sacsurveycheckerform/sacsurveycheckerform.component';
import { PermanentdisabilityComponent } from './Admin/Modules/permanentdisability/permanentdisability.component';
import { ApproverecordscountComponent } from './Admin/Modules/Deptreports/approverecordscount/approverecordscount.component';
import { SurveyedregcountsComponent } from './Admin/Modules/Deptreports/surveyedregcounts/surveyedregcounts.component';
import { AddvillageComponent } from './Admin/Modules/addvillage/addvillage.component';
import { LaunchComponent } from './Admin/Modules/launch/launch.component';
import { URRgisteredCountsComponent } from './Admin/Modules/Deptreports/urrgistered-counts/urrgistered-counts.component';
import { RegistrationsurveystatusreportComponent } from './Admin/Modules/Deptreports/registrationsurveystatusreport/registrationsurveystatusreport.component';
import { VillagedeletionComponent } from './Admin/Modules/Deptreports/villagedeletion/villagedeletion.component';
import { DistrictReportComponent } from './Admin/Modules/Deptreports/district-report/district-report.component';

/* import { NgFireworksModule } from '@fireworks-js/angular' */
 
@NgModule({
  declarations: [
    AppComponent,
    BadrequestComponent,
    LogoutComponent,
    SessionexpiredComponent,
    UnauthorizedaccessComponent,
    MenuheaderComponent,
    MenufooterComponent,

    AdminLayoutComponent,
    LayoutSidemenuComponent,
    MasterHeaderComponent,
    SacSurveyDashboardComponent,
    SacSurveyReportComponent,
    LoginComponent,
    DashboardComponent,
    ChangepasswordComponent,
    GenderReportComponent,
    CasteReportComponent,
    AgeReportComponent,
    HealthReportComponent,
    FemaleheadfamiliesComponent,
    EmploymentReportComponent,
    EducationalReportComponent,
    NewhomeComponent,
    StateHomeComponent,
    WorkActivityReportComponent,
    BarchartComponent,
    SacsurveycheckerformComponent,
    PermanentdisabilityComponent,
    ApproverecordscountComponent,
    SurveyedregcountsComponent,
    AddvillageComponent,
    LaunchComponent,
    URRgisteredCountsComponent,
    RegistrationsurveystatusreportComponent,
    VillagedeletionComponent,
    DistrictReportComponent
  ],
  imports: [
   
    FormsModule,
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    DataTablesModule,
    HttpClientModule, 
     NgxSpinnerModule.forRoot({ type: 'ball-scale-multiple' }),
    ToastrModule,
    NgApexchartsModule,
    QRCodeModule,
    GoogleMapsModule,
    MatAutocompleteModule,
    FormsModule,
    NgFor,
    ReactiveFormsModule,
    NgApexchartsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatCheckboxModule,
    CarouselModule,
    ExportAsModule,
  /*   NgFireworksModule, */
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-top-right',
      preventDuplicates : true,
      progressBar:true,
      progressAnimation : 'decreasing',
      tapToDismiss : true,
      maxOpened : 3,
      newestOnTop : true,

    }),
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot()
  ],
  providers: [SignalRServiceService],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class AppModule { }
