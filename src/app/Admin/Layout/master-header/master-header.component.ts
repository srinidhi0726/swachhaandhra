import { Component, ElementRef, Input, ViewChild } from '@angular/core'; 
import { OwlOptions } from 'ngx-owl-carousel-o';

import { ClientcallService } from 'src/app/service/clientcall.service';

import { SweetalertService } from 'src/app/thirdparty/sweetalert.service';
import { MidlayerService } from 'src/app/thirdparty/midlayer.service';
import Swal from 'sweetalert2'; 
import { ConsoleLogger } from '@microsoft/signalr/dist/esm/Utils';
import { Router } from '@angular/router';
import { FormArray, AbstractControl, FormControl, FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';

import { NgxSpinnerService } from 'ngx-spinner';
import {InputRequest,InputRequest2} from 'src/app/thirdparty/MasterObject'
import { CookieService } from 'ngx-cookie-service';
@Component({
  selector: 'app-master-header',
  templateUrl: './master-header.component.html',
  styleUrls: ['./master-header.component.css']
})
export class MasterHeaderComponent {
roleid:any='';
  activeButton: any
  showPhase(event: any){
    this.activeButton = event;
  }

  constructor(
     
    private router: Router,
    private alt: SweetalertService,
    private mid: MidlayerService,
    private apipost: ClientcallService,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private cookieService: CookieService,
 
  ) { 
    setInterval(() => {
      
    //  this.today = new Date(); 
      this.today= sessionStorage.getItem('lastlogintime');
      //this.pipe = new DatePipe('en-US');
      
    }, 1);
  }
  Name:any=sessionStorage.getItem('username');
  interval:any;  today: any;
  Istokenauth:boolean =true;
  ngOnInit(): void {
    this.roleid=sessionStorage.getItem("userrole");
    this.loginur=sessionStorage.getItem("userurtype");
    this.interval = setInterval(() => {
     // debugger;
     if(sessionStorage.getItem('_hsk') !='')
     {
     this.Sampletokencheckservice();
     }
    }, 5000);
  
  }

  loginur:any='';
  async Sampletokencheckservice(): Promise<void> {
   //debugger;
   const req = {
    type: "0"
  }
    let url= "api/swachaandhra/Sampletokencheck";
    let responce = await this.apipost.clinetposturlauth(req, url); 
    let rsdata = JSON.parse(this.mid.deccall(responce.data));
    //debugger;
    if(rsdata.code == false)
    {
    
      this.Istokenauth=false;
      sessionStorage.setItem('username', '');
      sessionStorage.setItem('_Uenc', '');
      sessionStorage.setItem('_hsk',  '');
      sessionStorage.setItem('_sltkn',  '');
      sessionStorage.setItem('lastlogintime', '');
   /*    this.router.navigate(["/NewHome"]); */
    }

  }

  
  async logout(): Promise<void> {
  debugger;

    let url= "api/swachaandhra/SwachhAndhra_logout";
    let responce = await this.apipost.clinetposturlauth('', url); 
    let rsdata = JSON.parse(this.mid.deccall(responce.data));
    console.log(rsdata.message)
  if(rsdata.code)
  {
   
    this.alt.success('Logout successfully.');
    this.Istokenauth=false;
    sessionStorage.setItem('username', '');
    sessionStorage.setItem('_Uenc', '');
    sessionStorage.setItem('_hsk',  '');
    sessionStorage.setItem('_sltkn',  '');
    sessionStorage.setItem('lastlogintime', '');
    
    this.router.navigate(["/NewHome"]);
  }
  else{
    
  }
  }

  urbanfun(U1:any)
  {

   sessionStorage.setItem('dist1',U1);
  }

  ngafteronIntiate()
  {
    // sessionStorage.setItem('dist1',U1);
    alert('hiii');
  }

}
   
