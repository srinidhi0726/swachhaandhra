import { Component } from '@angular/core';
import * as am5 from "@amcharts/amcharts5";
import * as am5xy from "@amcharts/amcharts5/xy";
import * as am5radar from "@amcharts/amcharts5/radar";
import am5themes_Animated from "@amcharts/amcharts5/themes/Animated";
@Component({
  selector: 'app-barchart',
  templateUrl: './barchart.component.html',
  styleUrls: ['./barchart.component.css']
})
export class BarchartComponent {
 
  ngOnInit(): void {
    this.vrtr();
  }

  
  
    xAxis1:any='';yAxis1:any='';legend1:any='';
    root1:any='';chart1:any='';datab:any []=[];
  vrt()
  {
    /* Chart code */

// Create root element
// https://www.amcharts.com/docs/v5/getting-started/#Root_element
this.root1 = am5.Root.new("chartdiv");


// Set themes
// https://www.amcharts.com/docs/v5/concepts/themes/
this.root1.setThemes([
  am5themes_Animated.new(this.root1)
]);


// Create chart
// https://www.amcharts.com/docs/v5/charts/xy-chart/
this.chart1 = this.root1.container.children.push(am5xy.XYChart.new(this.root1, {
  panX: false,
  panY: false,
  wheelX: "panX",
  wheelY: "zoomX",
  paddingLeft: 0,
  layout: this.root1.verticalLayout
}));

// Add scrollbar
// https://www.amcharts.com/docs/v5/charts/xy-chart/scrollbars/


this.datab = [{
  "year": "2021",
  "europe": 2.7
}, {
  "year": "2022",
  "namerica": 2.7
}, {
  "year": "2023",
  "asia": 2.4
}]


// Create axes
// https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
let xRenderer = am5xy.AxisRendererX.new(this.root1, {
 // minorGridEnabled: true
});
this.xAxis1 = this.chart1.xAxes.push(am5xy.CategoryAxis.new(this.root1, {
  categoryField: "year",
  renderer: xRenderer,
  tooltip: am5.Tooltip.new(this.root1, {})
}));

xRenderer.grid.template.setAll({
  location: 1
})

this.xAxis1.data.setAll(this.datab);

this.yAxis1 = this.chart1.yAxes.push(am5xy.ValueAxis.new(this.root1, {
  min: 0,
  max: 100,
  numberFormat: "#'%'",
  strictMinMax: true,
  calculateTotals: true,
  renderer: am5xy.AxisRendererY.new(this.root1, {
    strokeOpacity: 0.1
  })
}));


// Add legend
// https://www.amcharts.com/docs/v5/charts/xy-chart/legend-xy-series/
this.legend1 = this.chart1.children.push(am5.Legend.new(this.root1, {
  centerX: am5.p50,
  x: am5.p50
}));


// Add series
// https://www.amcharts.com/docs/v5/charts/xy-chart/series/


this.makeSeries1("Age 15-25", "europe");
this.makeSeries1("Age 26-35", "namerica");
this.makeSeries1("Age 36-45", "asia");
//this.makeSeries1("Age 46-60", "lamerica");

  }

  makeSeries1(name:any, fieldName:any) {
    let series = this.chart1.series.push(am5xy.ColumnSeries.new(this.root1, {
      name: name,
      stacked: true,
      xAxis: this.xAxis1,
      yAxis: this.yAxis1,
      valueYField: fieldName,
      valueYShow: "valueYTotalPercent",
      categoryXField: "year"
    }));
  
    series.columns.template.setAll({
      tooltipText: "{name}, {categoryX}:{valueYTotalPercent.formatNumber('#.#')}%",
      tooltipY: am5.percent(10)
    });
    series.data.setAll(this.datab);
  
    // Make stuff animate on load
    // https://www.amcharts.com/docs/v5/concepts/animations/
    series.appear();
  
  
  
    this.legend1.data.push(series);
  }

  HorBarchartreport()
  {
    debugger;
    /* Chart code */
// Create root element
// https://www.amcharts.com/docs/v5/getting-started/#Root_element
let root = am5.Root.new("chartdiv");

root.setThemes([
am5themes_Animated.new(root)
]);

let chart = root.container.children.push(am5xy.XYChart.new(root, {
panX: false,
panY: false,
wheelX: "panX",
layout: root.verticalLayout
}));

var data = [{ 
  category: "Research", 
  value:100,
  settings: {
    fill: am5.ColorSet.new(root, {
      colors: [
        am5.color(0x73556E),
        am5.color(0x9FA1A6),
        am5.color(0xF28F6B),
       // am5.color(0xFF6F91)
       am5.color(0xD65DB1)
      ]
      }).getIndex(4)
  }
}, { 
  category: "Marketing", 
  value: 200,
  settings: {
    fill: am5.ColorSet.new(root, {
      colors: [
        am5.color(0x73556E),
        am5.color(0x9FA1A6),
        am5.color(0xF28F6B),
       // am5.color(0xFF6F91)
       am5.color(0xD65DB1)
      ]
      }).getIndex(2)
  }
}, { 
  category: "Sales", 
  value:300,
  settings: {
    fill: am5.ColorSet.new(root, {
      colors: [
        am5.color(0x73556E),
        am5.color(0x9FA1A6),
        am5.color(0xF28F6B),
       // am5.color(0xFF6F91)
       am5.color(0xD65DB1)
      ]
      }).getIndex(1)
  }
}];


let yAxis = chart.yAxes.push(am5xy.CategoryAxis.new(root, {
  maxDeviation: 0.2,
categoryField: "category",
renderer: am5xy.AxisRendererY.new(root, {


})
}));


yAxis.data.setAll(data);

let xAxis = chart.xAxes.push(am5xy.ValueAxis.new(root, {
renderer: am5xy.AxisRendererX.new(root, {
  
})
}));



let series = chart.series.push(am5xy.ColumnSeries.new(root, {
  name:  "Series",
  xAxis: xAxis,
  yAxis: yAxis,
  valueXField: "value",
  categoryYField: "category",
  tooltip: am5.Tooltip.new(root, {
    pointerOrientation: "horizontal",
    labelText: "[bold][/]\n{categoryY}: {valueX}"
   })
}));




series.columns.template.setAll({
  templateField: "settings"
});

series.data.setAll(data);


var legend = chart.children.push(am5.Legend.new(root, {
  nameField: "categoryY",
  centerX: am5.percent(50),
  x: am5.percent(50)
}));

legend.data.setAll(series.dataItems);




  }

  vrtr()
  {
    var root = am5.Root.new("chartdiv"); 

root.setThemes([
  am5themes_Animated.new(root)
]);

var chart = root.container.children.push( 
  am5xy.XYChart.new(root, {
    panY: false,
    wheelY: "zoomX",
    layout: root.verticalLayout
  }) 
);

// Define data
var data = [{ 
  category: "Research", 
  value: 100,
  settings: {
    fill: am5.ColorSet.new(root, {
      colors: [
        am5.color(0x73556E),
        am5.color(0x9FA1A6),
        am5.color(0xF28F6B),
       // am5.color(0xFF6F91)
       am5.color(0xD65DB1)
      ]
      }).getIndex(4)
  }
}, { 
  category: "Marketing", 
  value: 200,
  settings: {
    fill: am5.ColorSet.new(root, {
      colors: [
        am5.color(0x73556E),
        am5.color(0x9FA1A6),
        am5.color(0xF28F6B),
       // am5.color(0xFF6F91)
       am5.color(0xD65DB1)
      ]
      }).getIndex(2)
  }
}, { 
  category: "Sales", 
  value:300,
  settings: {
    fill: am5.ColorSet.new(root, {
      colors: [
        am5.color(0x73556E),
        am5.color(0x9FA1A6),
        am5.color(0xF28F6B),
       // am5.color(0xFF6F91)
       am5.color(0xD65DB1)
      ]
      }).getIndex(1)
  }
}];

// Craete Y-axis
var yAxis = chart.yAxes.push(
  am5xy.ValueAxis.new(root, {
    renderer: am5xy.AxisRendererY.new(root, {})
  })
);

// Create X-Axis
var xAxis = chart.xAxes.push(
  am5xy.CategoryAxis.new(root, {
    maxDeviation: 0.2,
    renderer: am5xy.AxisRendererX.new(root, {
    }),
    categoryField: "category"
  })
);
xAxis.data.setAll(data);

// Create series
var series = chart.series.push( 
  am5xy.ColumnSeries.new(root, { 
    name: "Series", 
    xAxis: xAxis, 
    yAxis: yAxis, 
    valueYField: "value", 
    categoryXField: "category",
    tooltip: am5.Tooltip.new(root, {})
  }) 
);

series.columns.template.setAll({
  templateField: "settings"
});

series.data.setAll(data);

// Add legend
var legend = chart.children.push(am5.Legend.new(root, {
  nameField: "categoryX",
  centerX: am5.percent(50),
  x: am5.percent(50)
}));

legend.data.setAll(series.dataItems);
  }

   
}
