import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

import { ClientcallService } from 'src/app/service/clientcall.service';

import { SweetalertService } from 'src/app/thirdparty/sweetalert.service';
import { MidlayerService } from 'src/app/thirdparty/midlayer.service';
import Swal from 'sweetalert2';
import { ConsoleLogger } from '@microsoft/signalr/dist/esm/Utils';
import { Router } from '@angular/router';
 
import { DatePipe } from '@angular/common';
import { InputvalidaService } from 'src/app/thirdparty/inputvalida.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-urrgistered-counts',
  templateUrl: './urrgistered-counts.component.html',
  styleUrls: ['./urrgistered-counts.component.css']
})
export class URRgisteredCountsComponent {




  

    constructor(
  
      private router: Router,
      private alt: SweetalertService,
      private mid: MidlayerService,
      private apipost: ClientcallService,
      private spinner: NgxSpinnerService,
  
  
    ) {
    
    }
    roleid:any='';roledistid:any='';
    ngOnInit(): void {
      debugger;
      this.roleid=sessionStorage.getItem("userrole");
      if (
      (sessionStorage.getItem('username') == '')) {
  
       this.router.navigate(['/NewHome']); return;
      }
      this.Swatch_Ur='U';
      this.ischecked=true;
        this.GetURRegisteredCounts();
      }
      
   
  
    URchange(URID:any)
    {
     this.Swatch_Ur=URID;
     if(this.Swatch_Ur!='')
     {
      this.GetURRegisteredCounts();
     }
    }
    distarray: any[] = [];Swatch_DISTRICT:any='0';  dataRow:any []=[];
    Swatch_Ur:any='';
    ischecked:boolean=false;
    ischecked1:boolean=false;
   
    URCountdata: any[] = [];typeid:any='';
   
    async  GetURRegisteredCounts(): Promise<void> 
    {
      
      debugger;
        const req = {
          type: "207",
          input01:this.Swatch_Ur.toString(),
          
        }
        this.URCountdata=[];   
        let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
        // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
        let responce = await this.apipost.clinetposturlauth(req, urlservice);
        let rsdata = JSON.parse(this.mid.deccall(responce.data));
       
        this.URCountdata=rsdata.Details; 
       
       
      }
      Ur:any=''
      exportSlipXl() {
      const Data:any= this.URCountdata;
         
        
            debugger;
            //firstreport
  
            var body = [];
            var dataRow :any []=[];
            //Headers; 
            body.push(['','','','','SAFAIMITRA SURVEY - URBAN  OR RURAL REGISTERED DATA  REPORT','','','',''],
            );
            body.push(dataRow);
            if(this.Swatch_Ur=="U")
{
            body.push(['S.no',' District Name','District Code','Ulb Name','Ulb Code','Ward Name','Ward Code'],);
            for (let i = 0; i < Data.length; i++) {
              var dataRow = [];
              dataRow = [i+1,Data[i].DISTRICT_NAME,
              Data[i].DISTRICT_CODE,Data[i].MANDAL_NAME,Data[i].MANDAL_CODE,Data[i].VILLAGE_NAME,Data[i].VILLAGE_CODE],
              body.push(dataRow);
            }
             //footer
             body.push(['No Of Records:','','','','','',Data.length],
             );
}else{
  body.push(['S.no',' District Name','District Code','Mandal Name','Mandal Code','GP Name','GP Code','Village Name','Village Code'],);
   //body
   for (let i = 0; i < Data.length; i++) {
    var dataRow = [];
    dataRow = [i+1,Data[i].DISTRICT_NAME,
    Data[i].DISTRICT_CODE,Data[i].MANDAL_NAME,Data[i].MANDAL_CODE,Data[i].GP_NAME,Data[i].GP_CODE,Data[i].VILLAGE_NAME,Data[i].VILLAGE_CODE],
    body.push(dataRow);
  }
  body.push(['No Of Records:','','','','','','','',Data.length],
  );
}
           
           


            this.mid.JSONToCSVConvertor(
              body,
              this.Ur=="U" ?"Urban":"Rural" +'_'+'Registered Data Report',
              true
             );
      }
    }
   


