import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

import { ClientcallService } from 'src/app/service/clientcall.service';

import { SweetalertService } from 'src/app/thirdparty/sweetalert.service';
import { MidlayerService } from 'src/app/thirdparty/midlayer.service';
import Swal from 'sweetalert2';
import { ConsoleLogger } from '@microsoft/signalr/dist/esm/Utils';
import { Router } from '@angular/router';
 
import { DatePipe } from '@angular/common';
import { InputvalidaService } from 'src/app/thirdparty/inputvalida.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-district-report',
  templateUrl: './district-report.component.html',
  styleUrls: ['./district-report.component.css']
})
export class DistrictReportComponent {


    constructor(
  
      private router: Router,
      private alt: SweetalertService,
      private mid: MidlayerService,
      private apipost: ClientcallService,
      private spinner: NgxSpinnerService,
  
  
    ) {
    
    }
    roleid:any='';roledistid:any=''; urb1:any;
    ngOnInit(): void {
      debugger;
      this.roleid=sessionStorage.getItem("userrole");
      if (
      (sessionStorage.getItem('username') == '')) {
  
       this.router.navigate(['/NewHome']); return;
      }
      
      if (this.roleid=='1004' || this.roleid=='1003')
      { 
        this.Swatch_Ur=sessionStorage.getItem('userurtype');
        this.roledistid= sessionStorage.getItem('userdistcode');
        this.GetDistrictSurveyReportdata();
      }
      else   if (this.roleid=='1001' || this.roleid=='1002'|| this.roleid=='1006')
      {
        this.roledistid='0';
        this.Swatch_Ur="U";
        this.ischecked=true;
        this.GetDistrictSurveyReportdata();
      }
      
    this.urb1=(sessionStorage.getItem('dist1'));
alert(this.urb1);
    }
  
   


  
 



    URchange(URID:any)
    {
     this.Swatch_Ur=URID;
     if(this.Swatch_Ur!='')
     {
      this.GetDistrictSurveyReportdata();
     }
    }
    distarray: any[] = [];Swatch_DISTRICT:any='0';  dataRow:any []=[];
    Swatch_Ur:any="";
    ischecked:boolean=false;
    ischecked1:boolean=false;

    SurveyReportdata: any[] = [];typeid:any='';
    VillageSurveyReportdata: any[] = [];MandalSurveyReportdata: any[] = [];
    GpSurveyReportdata: any[] = [];
    async  GetDistrictSurveyReportdata(): Promise<void> 
    {
     
     
     
      debugger;
        const req = {
          type: "155",
          input01:this.Swatch_Ur.toString(),
          input02:this.roledistid.toString(),
        }
        this.SurveyReportdata=[];   
       
        let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
        // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
        let responce = await this.apipost.clinetposturlauth(req, urlservice);
        let rsdata = JSON.parse(this.mid.deccall(responce.data));
        let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
        this.SurveyReportdata=rsdata1.Details; 
        
         
      }
      

    
      
   

  
      
      exportSlipXl(){}
   
  //   exportSlipXl() {
      
  //     if (this.Swatch_Ur == 'U') {
  //       this.Ur = 'Urban';
  //       this.mandalname='Urban Local Body';
  //       this.villagename='Ward';
  //     }
  //     else if (this.Swatch_Ur == 'R') {
  //       this.Ur = 'Rural';
  //       this.mandalname='Mandal';
  //       this.villagename='Gram panchayat';
  //     }
      
  //     if(this.Reportlevel =='0')
  //     {
  //       this.Data1= this.SurveyReportdata;
  //     }
  //     if(this.Reportlevel =='1')
  //     {
  //       this.Data1= this.MandalSurveyReportdata;
  //     }
  //     if(this.Reportlevel =='2')
  //     {
  //       this.Data1= this.GpSurveyReportdata;
  //     }
  //     if(this.Reportlevel =='3')
  //     {
  //       this.Data1= this.VillageSurveyReportdata;
  //     }
  //     const Data=this.Data1;
  //   if(this.SurveyReportdata.length > 0)
  //   {
  //     const checkdata = Math.max(Data.length);
  //     if(checkdata>0){
  //       debugger;
  //       //firstreport
    
  //       var body = [];
  //       var dataRow :any []=[];
  //       //Headers; 
  //       body.push(['','','','SAFAIMITRA SURVEY - APPROVE/REJECT COUNT REPORT','','',''],
  //       );
  //       body.push(dataRow);
  //       if(this.Reportlevel =='1')
  //       {
  //         body.push(['','District :',this.District,'','','',''],
  //         );
  //       }
  //       else if(this.Reportlevel =='2')
  //       {
  //         body.push(['','District :',this.District,'Mandal :',this.Mandal,'','','' ],
  //         );
  //       }
  //       else if(this.Reportlevel =='3')
  //       {
  //         body.push(['', 'District :',this.District, 'Mandal :',this.Mandal, 'Grampanchayat/Ward :',this.Gp,'',''],
  //         );
  //       }
       
      
  //       if(this.Reportlevel =='0')
  //       {   
  //         if (this.Swatch_Ur == 'R') {
  //           body.push(['S.No',
  //           'District',
  //           'Total Surveyed',
  //           'EOPRRD to be Approved',
  //           'EOPRRD Rejected',
  //           'DPO to be Approved',
  //           'Total Approved'],);
  //         }
  //         else  {
  //           body.push(['S.No',
  //           'District',
  //           'Total Surveyed',
  //           'Nodal Officer to be Approved',
  //           'Nodal Officer Rejected',
  //           'Municipal Commissioner to be Approved',
  //           'Total Approved'],);
  //         }
       
  //       //body
  //       for (let i = 0; i < Data.length; i++) {
  //         var dataRow = [];
  //         dataRow = [Data[i].DISTRICT_NAME !="Total" ? i+1:"",Data[i].DISTRICT_NAME,
  //         Data[i].SUBMITTED_COUNT,
  // Data[i].NODAL_PENDING_COUNT,
  // Data[i].NODAL_REJECT_COUNT,
  // Data[i].MC_PENDING_COUNT,
  // Data[i].FINAL_APPROVAL_COMPLETED
  //       ];
  //         body.push(dataRow);
  //       }
  //       if (this.Swatch_Ur == 'R') {
  //       body.push(['No Of Records:','','','','','',(Data.length-1)],
  //       );
  //       }else{
  //         body.push(['No Of Records:','','','','','',(Data.length-1)], );
  //       }
  //     }
  
  //     else  if(this.Reportlevel =='1')
  //     {   
  //         if (this.Swatch_Ur == 'R') {
  //           body.push(['S.No','Mandal', 'Total Surveyed',
  //           'EOPRRD to be Approved',
  //           'EOPRRD Rejected',
  //           'DPO to be Approved',
  //           'Total Approved'],);
  //         }
  //         else  {
  //           body.push(['S.No','Urban Local Body','Urban Local Body Code',
  //           'Total Surveyed',
  //           'EOPRRD to be Approved',
  //           'EOPRRD Rejected',
  //           'DPO to be Approved',
  //           'Total Approved'],);
  //         }
       
  //       //body
  //       for (let i = 0; i < Data.length; i++) {
  //         var dataRow = [];
  //         if(this.Swatch_Ur == 'R'){
  //         dataRow = [Data[i].MANDAL_NAME !="Total" ? i+1:"",Data[i].MANDAL_NAME,
  //         Data[i].SUBMITTED_COUNT,
  // Data[i].NODAL_PENDING_COUNT,
  // Data[i].NODAL_REJECT_COUNT,
  // Data[i].MC_PENDING_COUNT,
  // Data[i].FINAL_APPROVAL_COMPLETED
  //       ];
  //     }
  //       else{
  //         dataRow = [Data[i].MANDAL_NAME !="Total" ? i+1:"",Data[i].MANDAL_NAME,Data[i].MANDAL_NAME !="Total" ? Data[i].MANDAL_ID:"",
  //         Data[i].SUBMITTED_COUNT,
  // Data[i].NODAL_PENDING_COUNT,
  // Data[i].NODAL_REJECT_COUNT,
  // Data[i].MC_PENDING_COUNT,
  // Data[i].FINAL_APPROVAL_COMPLETED
  //       ];
  //     }
  //         body.push(dataRow);
  //       }
  //       if(this.Swatch_Ur == 'R'){
  //       body.push(['No Of Records:','','','','','',(Data.length-1)],
  //       );}
  //       else{
  //         body.push(['No Of Records:','','','','','','',(Data.length-1)],
  //         );
  //       }
  //     }
  
  
  //     else  if(this.Reportlevel =='2')
  //     {   
  //         if (this.Swatch_Ur == 'R') {
  //           body.push(['S.No','Gram Panchayat','GP(LGD)Code', 'Total Surveyed',
  //           'EOPRRD to be Approved',
  //           'EOPRRD Rejected',
  //           'DPO to be Approved',
  //           'Total Approved'],);
  //         }
  //         else  {
  //           body.push(['S.No','Ward', 'Total Surveyed',
  //           'EOPRRD to be Approved',
  //           'EOPRRD Rejected',
  //           'DPO to be Approved',
  //           'Total Approved'],);
  //         }
       
  //       //body
  //       for (let i = 0; i < Data.length; i++) {
  //         var dataRow = [];
  //         if(this.Swatch_Ur == 'R'){
  //           dataRow = [Data[i].WARD_OR_VILLAGE_NAME !="Total" ? i+1:"",Data[i].WARD_OR_VILLAGE_NAME,Data[i].WARD_OR_VILLAGE_NAME !="Total" ? Data[i].WARD_OR_VILLAGE_ID:"",
  //           Data[i].SUBMITTED_COUNT,
  //           Data[i].NODAL_PENDING_COUNT,
  //           Data[i].NODAL_REJECT_COUNT,
  //           Data[i].MC_PENDING_COUNT,
  //           Data[i].FINAL_APPROVAL_COMPLETED
  //       ];
  //     }
  //       else{
          
  //         dataRow = [Data[i].WARD_OR_VILLAGE_NAME !="Total" ? i+1:"",Data[i].WARD_OR_VILLAGE_NAME,Data[i].SUBMITTED_COUNT,
  //         Data[i].NODAL_PENDING_COUNT,
  //         Data[i].NODAL_REJECT_COUNT,
  //         Data[i].MC_PENDING_COUNT,
  //         Data[i].FINAL_APPROVAL_COMPLETED
  //       ];
  //     }
  //         body.push(dataRow);
        
  //       }
  //       if(this.Swatch_Ur == 'R'){
  //         body.push(['No Of Records:','','','','','','',(Data.length-1)],
  //         );}
  //         else{
  //           body.push(['No Of Records:','','','','','',(Data.length-1)],
  //           );
  //         }
  //     }
  
  
  
  //   else  if(this.Reportlevel =='3')
  //   {  
  //     if(this.Swatch_Ur == 'R'){ 
  //   body.push(['S.No','Village',
  //   'Village Revenue Code ', 'Total Surveyed',
  //   'EOPRRD to be Approved',
  //   'EOPRRD Rejected',
  //   'DPO to be Approved',
  //   'Total Approved'],);
  //     }
  //   //body
  //   for (let i = 0; i < Data.length; i++) {
  //     var dataRow = [];
  //     dataRow = [Data[i].WARD_OR_VILLAGE_NAME !="Total" ? i+1:"",Data[i].WARD_OR_VILLAGE_NAME,  
  //     Data[i].WARD_OR_VILLAGE_ID,Data[i].SUBMITTED_COUNT,
  //     Data[i].NODAL_PENDING_COUNT,
  //     Data[i].NODAL_REJECT_COUNT,
  //     Data[i].MC_PENDING_COUNT,
  //     Data[i].FINAL_APPROVAL_COMPLETED
  //   ];
  // }
  //     body.push(dataRow);
  //     if(this.Swatch_Ur == 'R'){
  //       body.push(['No Of Records:','','','',,'','','',(Data.length-1)],
  //       );}
  //       else{
  //         body.push(['No Of Records:','','','','',(Data.length-1)],
  //         );
  //       }
   
  //   }
  //       //footer
      
    
  //       if(this.Reportlevel =='0')
  //       {
  //         this.mid.JSONToCSVConvertor(
  //           body,
  //           'District Level'+'_'+this.Ur+'_'+'Surveyor Login Registration Count Report Data',
  //           true
  //           );
  //       }
  //       if(this.Reportlevel =='1')
  //       {
  //         this.mid.JSONToCSVConvertor(
  //           body,
  //           this.District +'_'+this.Ur+'_'+ 'Surveyor Login Registration Count Report Data',
  //           true
  //           );
  //       }
  //       if(this.Reportlevel =='2')
  //       {
  //         this.mid.JSONToCSVConvertor(
  //           body,
  //           this.Mandal +'_'+this.Ur+'_'+'Surveyor Login Registration Count Report Data',
  //           true
  //           );
  //       }
  //       if(this.Reportlevel =='3')
  //       {
  //         this.mid.JSONToCSVConvertor(
  //           body,
  //           this.Gp +'_'+this.Ur+'_'+'Surveyor Login Registration Count Report Data',
  //           true
  //           );
  //       }
      
  //     }
  //   }
  //   }

    addModaldisplay:any='none';
    modalclose()
    {
      this.addModaldisplay = 'none';
    }
  
   
   
  }