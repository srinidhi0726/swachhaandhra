import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

import { ClientcallService } from 'src/app/service/clientcall.service';

import { SweetalertService } from 'src/app/thirdparty/sweetalert.service';
import { MidlayerService } from 'src/app/thirdparty/midlayer.service';
import Swal from 'sweetalert2';
import { ConsoleLogger } from '@microsoft/signalr/dist/esm/Utils';
import { Router } from '@angular/router';
 
import { DatePipe } from '@angular/common';
import { InputvalidaService } from 'src/app/thirdparty/inputvalida.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-employment-report',
  templateUrl: './employment-report.component.html',
  styleUrls: ['./employment-report.component.css']
})
export class EmploymentReportComponent {
 

    constructor(
  
      private router: Router,
      private alt: SweetalertService,
      private mid: MidlayerService,
      private apipost: ClientcallService,
      private spinner: NgxSpinnerService,
  
  
    ) {
    
    }
    roleid:any='';roledistid:any='';
    ngOnInit(): void {
      debugger;
      this.roleid=sessionStorage.getItem("userrole");
      if (
      (sessionStorage.getItem('username') == '')) {
  
       this.router.navigate(['/NewHome']); return;
      }
     
      if (this.roleid=='1004' || this.roleid=='1003')
      { 
        this.Swatch_Ur=sessionStorage.getItem('userurtype');
        this.roledistid= sessionStorage.getItem('userdistcode');
        this.GetSurveyReportdata();
      }
      else   if (this.roleid=='1001' || this.roleid=='1002'|| this.roleid=='1006')
      {
        this.roledistid='0';
        this.Swatch_Ur="U";
        this.ischecked=true;
        this.GetSurveyReportdata();
      }
     
    }
  
    URchange(URID:any)
    {
     this.Swatch_Ur=URID;
     if(this.Swatch_Ur!='')
     {
      this.GetSurveyReportdata();
     }
    }
    distarray: any[] = [];Swatch_DISTRICT:any='0';  dataRow:any []=[];
    Swatch_Ur:any="";
    ischecked:boolean=false;
    ischecked1:boolean=false;
    VillageSurveyReportdata: any[] = [];MandalSurveyReportdata: any[] = [];
    GpSurveyReportdata: any[] = []; Reportlevel:any=0;
    SurveyReportdata: any[] = [];typeid:any='';
    async  GetSurveyReportdata(): Promise<void> 
    {
      sessionStorage.setItem('dist', '');
      sessionStorage.setItem('distname', '');
      sessionStorage.setItem('mandal', '');
      sessionStorage.setItem('mandalname', '');
      this.Reportlevel=0;
      debugger;
        const req = {
          type: "125",
          input01:this.Swatch_Ur.toString(),
          input02:this.roledistid.toString(),
        }
        this.SurveyReportdata=[];   
        let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
        // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
        let responce = await this.apipost.clinetposturlauth(req, urlservice);
        let rsdata = JSON.parse(this.mid.deccall(responce.data));
        let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
        console.log(rsdata.Details);
        this.SurveyReportdata=rsdata.Details;
        this.sumtotalrows(rsdata1.Details,"DISTRICT_NAME");
      this.SurveyReportdata.push(this.details);
      }
      details:any[]=[];
      async  sumtotalrows(details:any,column:any): Promise<void> 
      {
        const scores =details; 
     
        const sumScores = (arr:any) => {
          return arr.reduce((acc:any, val:any) => {
            acc[column] = 'Total';
             Object.keys(val).forEach(key => {
              /*   if((key!== 'DISTRICT_NAME1')){ */
                   acc[key] += val[key];
               /*  }; */
             });
           /*   if((acc['DISTRICT_NAME'] !== 'all')){ */
                acc[column] = 'Total';
             /* }; */
             return acc;
          });
         
       };
       
       debugger;
       let det=sumScores(details);
       if(details.length ==1)
       {
        det[column]='Total';
       }
      for(var k in det) {
        if(k != "0")
        {
          delete scores[k];
        }
        
     } 
     this.details=det;
      }
      async  GetMandalSurveyReportdata1(details:any): Promise<void> 
      {
        let rstdata:any =[];
        const scores =rstdata; 
     
        const sumScores = (arr:any) => {
          return arr.reduce((acc:any, val:any) => {
             Object.keys(val).forEach(key => {
                if((key!== 'DISTRICT_NAME') && (key!== 'STATUS_TEXT')){
                   acc[key] += val[key];
                };
             });
             if((acc['DISTRICT_NAME'] !== 'all') && (acc['STATUS_TEXT'] !== 'all')){
                acc['DISTRICT_NAME'] = 'Total';
             };
             return acc;
          });
       };
//let rtsdata:any=sumScores(scores);
       for(var k in scores) {
        if(k != "0")
        {
          console.log(k, scores[k]);
          delete scores[k];
        }
        debugger;
     //   console.log(rtsdata);
     }
      }
      District:any='';Mandal:any='';Gp:any='';
      async  GetMandalSurveyReportdata(Distrctcode:any,Distname:any): Promise<void> 
    {
      this.District=Distname;
      sessionStorage.setItem('dist', Distrctcode);
      sessionStorage.setItem('distname', Distname);
      this.Reportlevel++;
      let rolemandalcode:any='';
      if (this.roleid=='1004' || this.roleid=='1003')
      {
        if(this.Swatch_Ur.toString() =="U")
        {
          rolemandalcode=sessionStorage.getItem('userulbcode');
        }
        else  if(this.Swatch_Ur.toString() =="R")
        {
          if(this.roleid=='1003')
        {
          rolemandalcode="0";
        }
        else{
          rolemandalcode=sessionStorage.getItem('usermandalcode');
        }
         
        }
       
      }
      else{
        rolemandalcode='0';
      }
      debugger;
        const req = {
          type:  this.Swatch_Ur.toString() =="U" ?"129":"126",
          input01:Distrctcode.toString(),
          input02:this.Swatch_Ur.toString(),
          input03:rolemandalcode.toString(),
        }
        this.MandalSurveyReportdata=[];   
        let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
        // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
        let responce = await this.apipost.clinetposturlauth(req, urlservice);
        let rsdata = JSON.parse(this.mid.deccall(responce.data));
        let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
        if(rsdata.code)
        {
          this.MandalSurveyReportdata=rsdata.Details; 
          this.sumtotalrows(rsdata1.Details,"MANDAL_NAME");
          this.MandalSurveyReportdata.push(this.details);
        }
        else{
          this.alt.toasterror('No data found')
        }
        
      }
  
      async  GetGpSurveyReportdata(mandalname:any,Mandalcode:any): Promise<void> 
    {
      this.District=sessionStorage.getItem('distname');
      this.Mandal=mandalname;
      sessionStorage.setItem('mandal', Mandalcode);
      sessionStorage.setItem('mandalname', mandalname);
      this.Reportlevel++;
      let rolegpcode:any='';
      if (this.roleid=='1004' || this.roleid=='1003')
    {
      if(this.Swatch_Ur.toString() =="U")
      {
        rolegpcode='0';
      }
      else  if(this.Swatch_Ur.toString() =="R")
      {
       
        rolegpcode="0";
        
      }
     
    }
    else{
      rolegpcode='0';
    }
      debugger;
      const req = {
        type: this.Swatch_Ur.toString() =="U" ?"130":"127",
        input01:sessionStorage.getItem('dist'),
        input02:Mandalcode.toString(),
        input03:this.Swatch_Ur.toString(),
        input04:rolegpcode.toString(),
      }
        this.GpSurveyReportdata=[];   
        let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
        // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
        let responce = await this.apipost.clinetposturlauth(req, urlservice);
        let rsdata = JSON.parse(this.mid.deccall(responce.data));
        let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
        console.log(rsdata.Details);
        this.GpSurveyReportdata=rsdata.Details; 
        this.sumtotalrows(rsdata1.Details,"WARD_OR_VILLAGE_NAME");
        this.GpSurveyReportdata.push(this.details);
      }
  
      async  GetVillageSurveyReportdata(Gpname:any,gpcode:any): Promise<void> 
    {
      this.Gp=Gpname;
      this.Reportlevel++;
      debugger;
        const req = {
          type: "128",
          input01:sessionStorage.getItem('dist'),
          input02:sessionStorage.getItem('mandal'),
          input03:gpcode.toString(),
          input04:this.Swatch_Ur.toString(),
        }
        this.VillageSurveyReportdata=[];   
        let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
        // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
        let responce = await this.apipost.clinetposturlauth(req, urlservice);
        let rsdata = JSON.parse(this.mid.deccall(responce.data));
        let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
        console.log(rsdata.Details);
        this.VillageSurveyReportdata=rsdata.Details; 
        this.sumtotalrows(rsdata1.Details,"WARD_OR_VILLAGE_NAME");
        this.VillageSurveyReportdata.push(this.details);
      }
    
      Back()
      {
        this.Reportlevel--;
      }
  
      Data1:any[]=[]; Ur:any=''
      mandalname='';
      villagename='';
  exportSlipXl() {
   
    if (this.Swatch_Ur == 'U') {
      this.Ur = 'Urban';
      this.mandalname='Urban Local Body';
      this.villagename='Ward';
    }
    else if (this.Swatch_Ur == 'R') {
      this.Ur = 'Rural';
      this.mandalname='Mandal';
      this.villagename='Gram panchayat';
    }
      if(this.Reportlevel =='0')
      {
        this.Data1= this.SurveyReportdata;
      }
      if(this.Reportlevel =='1')
      {
        this.Data1= this.MandalSurveyReportdata;
      }
      if(this.Reportlevel =='2')
      {
        this.Data1= this.GpSurveyReportdata;
      }
      if(this.Reportlevel =='3')
      {
        this.Data1= this.VillageSurveyReportdata;
      }
      const Data=this.Data1;
  if(this.SurveyReportdata.length > 0)
  {
      const checkdata = Math.max(Data.length);
      if(checkdata>0){
        debugger;
        //firstreport
  
        var body = [];
        var dataRow :any []=[];
        //Headers; 
        body.push(['','','','','','','','','SAFAIMITRA SURVEY - EMPLOYMENT DATA REPORTS','','','','','','','','',''],
        );
        body.push(dataRow);
        if(this.Reportlevel =='1')
        {
          body.push(['','','District :',this.District,'','','','','','','','','','','','','',''],
          );
        }
        else if(this.Reportlevel =='2')
        {
          body.push(['','','District :',this.District,'','','Mandal :',this.Mandal,'','','','','','','','','',''],
          );
        }
        else if(this.Reportlevel =='3')
        {
          body.push(['','','District :',this.District,'','','Mandal :',this.Mandal,'','','Grampanchayat/Ward :',this.Gp,'','','','','',''],
          );
        }
        body.push(['','','','','','','','','','','','Genderwise-Employment','','','','',''],
        );
      
        body.push(['','','','Type Of Employment','','','Private Sector','','','Contract','','','Out Sourcing','','','Permanent Employee',''],
        );
        if(this.Reportlevel =='0')
        {  
        body.push(['S.no','District',
        'Private Sector','Contract','Out Sourcing','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender'],);
        //body
        for (let i = 0; i < Data.length; i++) {
          var dataRow = [];
          dataRow = [i+1,Data[i].DISTRICT_NAME,
          Data[i].PRIVATE_SECTOR,Data[i].CONTRACT,Data[i].OUT_SOURCING,Data[i].M_PRIVATE_SECTOR,Data[i].F_PRIVATE_SECTOR,
          Data[i].T_PRIVATE_SECTOR,Data[i].M_CONTRACT,Data[i].F_CONTRACT,Data[i].T_CONTRACT,Data[i].M_OUT_SOURCING,
          Data[i].F_OUT_SOURCING,Data[i].T_OUT_SOURCING,Data[i].M_PERMANENT_EMPLOYEE,Data[i].F_PERMANENT_EMPLOYEE,Data[i].T_PERMANENT_EMPLOYEE,
         
        ];
          body.push(dataRow);
        }
      }
      else  if(this.Reportlevel =='1')
      {  
      body.push(['S.no',this.mandalname,
      'Private Sector','Contract','Out Sourcing','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender'],);
      //body
      for (let i = 0; i < Data.length; i++) {
        var dataRow = [];
        dataRow = [i+1,Data[i].MANDAL_NAME,
        Data[i].PRIVATE_SECTOR,Data[i].CONTRACT,Data[i].OUT_SOURCING,Data[i].M_PRIVATE_SECTOR,Data[i].F_PRIVATE_SECTOR,
        Data[i].T_PRIVATE_SECTOR,Data[i].M_CONTRACT,Data[i].F_CONTRACT,Data[i].T_CONTRACT,Data[i].M_OUT_SOURCING,
        Data[i].F_OUT_SOURCING,Data[i].T_OUT_SOURCING,Data[i].M_PERMANENT_EMPLOYEE,Data[i].F_PERMANENT_EMPLOYEE,Data[i].T_PERMANENT_EMPLOYEE,
       
      ];
        body.push(dataRow);
      }
    }
    else  if(this.Reportlevel =='2')
      {  
      body.push(['S.no',this.villagename,
      'Private Sector','Contract','Out Sourcing','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender'],);
      //body
      for (let i = 0; i < Data.length; i++) {
        var dataRow = [];
        if(this.Mandal !='Penamaluru' && this.Swatch_Ur =='U')
        {
        dataRow = [i+1,"Ward No"+Data[i].WARD_OR_VILLAGE_NAME,
        Data[i].PRIVATE_SECTOR,Data[i].CONTRACT,Data[i].OUT_SOURCING,Data[i].M_PRIVATE_SECTOR,Data[i].F_PRIVATE_SECTOR,
        Data[i].T_PRIVATE_SECTOR,Data[i].M_CONTRACT,Data[i].F_CONTRACT,Data[i].T_CONTRACT,Data[i].M_OUT_SOURCING,
        Data[i].F_OUT_SOURCING,Data[i].T_OUT_SOURCING,Data[i].M_PERMANENT_EMPLOYEE,Data[i].F_PERMANENT_EMPLOYEE,Data[i].T_PERMANENT_EMPLOYEE,
       
      ];
    }
    else{
      dataRow = [i+1,Data[i].WARD_OR_VILLAGE_NAME,
        Data[i].PRIVATE_SECTOR,Data[i].CONTRACT,Data[i].OUT_SOURCING,Data[i].M_PRIVATE_SECTOR,Data[i].F_PRIVATE_SECTOR,
        Data[i].T_PRIVATE_SECTOR,Data[i].M_CONTRACT,Data[i].F_CONTRACT,Data[i].T_CONTRACT,Data[i].M_OUT_SOURCING,
        Data[i].F_OUT_SOURCING,Data[i].T_OUT_SOURCING,Data[i].M_PERMANENT_EMPLOYEE,Data[i].F_PERMANENT_EMPLOYEE,Data[i].T_PERMANENT_EMPLOYEE,
       
      ];
    }
        body.push(dataRow);
      }
    }
    else  if(this.Reportlevel =='3')
      {  
      body.push(['S.no','Village',
      'Private Sector','Contract','Out Sourcing','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender'],);
      //body
      for (let i = 0; i < Data.length; i++) {
        var dataRow = [];
        dataRow = [i+1,Data[i].WARD_OR_VILLAGE_NAME,
        Data[i].PRIVATE_SECTOR,Data[i].CONTRACT,Data[i].OUT_SOURCING,Data[i].M_PRIVATE_SECTOR,Data[i].F_PRIVATE_SECTOR,
        Data[i].T_PRIVATE_SECTOR,Data[i].M_CONTRACT,Data[i].F_CONTRACT,Data[i].T_CONTRACT,Data[i].M_OUT_SOURCING,
        Data[i].F_OUT_SOURCING,Data[i].T_OUT_SOURCING,Data[i].M_PERMANENT_EMPLOYEE,Data[i].F_PERMANENT_EMPLOYEE,Data[i].T_PERMANENT_EMPLOYEE,
       
      ];
        body.push(dataRow);
      }
    }
        //footer
        body.push(['No Of Records:','','','','','','','','','','','','','','','',Data.length],
        );
  
        if(this.Reportlevel =='0')
        {
          this.mid.JSONToCSVConvertor(
            body,
            'District Level'+'_'+this.Ur+'_'+'Employment Report Data',
            true
           );
        }
        if(this.Reportlevel =='1')
        {
          this.mid.JSONToCSVConvertor(
            body,
           this.District +'_'+this.Ur+'_'+ 'Employment Report Data',
            true
           );
        }
        if(this.Reportlevel =='2')
        {
          this.mid.JSONToCSVConvertor(
            body,
            this.Mandal +'_'+this.Ur+'_'+'Employment Report Data',
            true
           );
        }
        if(this.Reportlevel =='3')
        {
          this.mid.JSONToCSVConvertor(
            body,
            this.Gp +'_'+this.Ur+'_'+'Employment Report Data',
            true
           );
        }
  
     
      }
    }
    }
   
  }