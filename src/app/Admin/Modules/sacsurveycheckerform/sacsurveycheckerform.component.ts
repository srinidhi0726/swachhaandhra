import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

import { ClientcallService } from 'src/app/service/clientcall.service';

import { SweetalertService } from 'src/app/thirdparty/sweetalert.service';
import { MidlayerService } from 'src/app/thirdparty/midlayer.service';
import Swal from 'sweetalert2';
import { ConsoleLogger } from '@microsoft/signalr/dist/esm/Utils';
import { Router } from '@angular/router';
 
import { DatePipe } from '@angular/common';
import { InputvalidaService } from 'src/app/thirdparty/inputvalida.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CookieService } from 'ngx-cookie-service';
import { any } from 'cypress/types/bluebird';

@Component({
  selector: 'app-sacsurveycheckerform',
  templateUrl: './sacsurveycheckerform.component.html',
  styleUrls: ['./sacsurveycheckerform.component.css']
})
export class SacsurveycheckerformComponent {

  constructor(

    private router: Router,
    private alt: SweetalertService,
    private mid: MidlayerService,
    private apipost: ClientcallService,
    private spinner: NgxSpinnerService,


  ) {
  
  }
  roleid:any='';
  ngOnInit(): void {
    this.roleid=sessionStorage.getItem("userrole");
    debugger;
    if ((sessionStorage.getItem('username') == '') 
    ) {

      this.router.navigate(['/NewHome']); return;
    }
    if(this.roleid=='1005' )
    {this.Swatch_DISTRICT= sessionStorage.getItem('userdistcode');
    this.Swatch_MMC_CODE=sessionStorage.getItem('usermandalcode');
    this.Swatch_Ur=sessionStorage.getItem('userurtype');
    this.Swatch_GP_CODE=sessionStorage.getItem('usergpcode');
    this.Swatch_VW_CODE=sessionStorage.getItem('uservillagecode');

 
    
this.GetSurveyReportdata();
    }
    else if (this.roleid=='1004' || this.roleid=='1003')
    {
      
debugger;
      this.Swatch_DISTRICT= sessionStorage.getItem('userdistcode');

    this.Swatch_Ur=sessionStorage.getItem('userurtype');
    
    if(this.Swatch_Ur=='R')
    {
      this.Levelthree='Gram Panchayat';
      if(this.roleid=='1003')
      {
        this.Leveltwo='Mandal';
        this.mandalload();
      }
else
{
  this.Levelthree='Gram Panchayat';
//this.Swatch_GP_CODE=sessionStorage.getItem('usergpcode');
//this.villageload();
this.Swatch_MMC_CODE=sessionStorage.getItem('usermandalcode');
this.gpload();
}

    }
    else if(this.Swatch_Ur=='U')
    {
      this.Leveltwo='Urban Local Body';
      this.Levelthree='Wards';
      this.Swatch_MMC_CODE=sessionStorage.getItem('userulbcode');
      this.gpload();
     
    }
    }
    else if (this.roleid=='1001' || this.roleid=='1002' || this.roleid=='1006'){
      this.districtload();
    }
  
  }
  Leveltwo:any='Urban Local Body';
Levelthree:any='Wards';
  URchange(URID:any)
  {
   this.Swatch_Ur=URID;
   if(this.Swatch_Ur!='')
   {
    if(this.Swatch_Ur=='R')
    {
this.Leveltwo='Mandal';
this.Levelthree='Gram Panchayat';
    }
    else 
    {
      this.Leveltwo='Urban Local Body';
      this.Levelthree='Wards';
    }
    this.mandalload();
   }
  }
  distarray: any[] = [];Swatch_DISTRICT:any='0';  dataRow:any []=[];
  Swatch_Ur:any="";
  ischecked:boolean=false;
  ischecked1:boolean=false;
  async districtload(): Promise<void> {
    
    try {
      const req = {
        type: '101'
      };
      this.SurveyReportdata=[];  
    this.Swatch_DISTRICT='0';
    this.Swatch_MMC_CODE = '0';
    this.Swatch_VW_CODE = '0';
    this.Swatch_GP_CODE = '0';
    this.Swatch_Ur='U';
    this.ischecked=true;
      this.distarray = []; this.spinner.show();
      this.mandalarry = [];
      this.villagearry = [];
      this.gparry=[];
   /*    this.Masters.value.STORE_MMC_CODE = '0';
      this.Masters.value.STORE_VW_CODE = '0'; */
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      this.spinner.hide();


      this.distarray = rsdata.Details;
      let Data =this.distarray;
     debugger;
     for(var i in Data) {    

      var item = Data[i];   
  
      this.dataRow.push({ 
          "firstName" :Data[i].DISTRICT_ID,
          "lastName"  : Data[i].DISTRICT_NAME
      });
  }
     
      let obj =[
        { value: 100, category: "Total" },
        { value: 50, category: "Male" },
        { value: 40, category: "Female" },
        { value: 10, category: "Transgender" },
      ];
      console.log(obj);
      console.log(this.dataRow);
      this.spinner.hide();

    } catch (error) {

    }
  }
  mandalarry: any[] = []; Swatch_MMC_CODE:any='0';Swatch_VW_CODE:any='0';
   async mandalload(): Promise<void> {
    try {
      let typeid:any='';
     if(this.Swatch_Ur=='U') 
     {
      this.ischecked=true;
      this.typeid='115';
     }
     else
     {
      this.ischecked=false;
      this.typeid='102';
     }
     
   
      if(this.Swatch_Ur!='')
      {

      
      if(this.Swatch_DISTRICT !='0')
      {
      const req = {
        type: this.typeid.toString(),
        input01:this.Swatch_Ur.toString(),
        input02:this.Swatch_DISTRICT.toString()
      };
      this.SurveyReportdata=[];  
        this.Swatch_MMC_CODE = '0';
        this.Swatch_VW_CODE = '0';
        this.Swatch_GP_CODE = '0';
      this.mandalarry = []; this.spinner.show();
      this.villagearry = [];
this.gparry=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      this.spinner.hide();

     
      if(rsdata.code)
      {
        this.mandalarry = rsdata.Details;
      }
      else{
        this.alt.toasterror('No data found')
      }
      this.spinner.hide();
      }
     }
     else{
      this.alt.toastwarning('Please select Urban/Rural')
     }

    } catch (error) {

    }
  } 
  villagearry: any[] = [];  gparry: any[] = [];Swatch_GP_CODE:any='0';isgetdata:boolean=false;
  async gpload(): Promise<void> {
    try {
      let typeid:any='';
      if(this.Swatch_Ur=='U') 
      {
      
       this.typeid='116';
      }
      else
      {
     
       this.typeid='103';
      }
      if((this.Swatch_DISTRICT !='0')&& (this.Swatch_MMC_CODE !='0'))
      {
      const req = {
        type: this.typeid.toString(),
        input01:this.Swatch_Ur.toString(),
        input02:this.Swatch_DISTRICT.toString(),
        input03:this.Swatch_MMC_CODE.toString()
      };

      this.SurveyReportdata=[];  
        this.Swatch_GP_CODE = '0';
        this.Swatch_VW_CODE = '0';
      this.gparry = []; this.spinner.show();
this.villagearry=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));

      this.spinner.hide();

      this.gparry = rsdata.Details;

    }

    } catch (error) {

    }
  } 

 async villageload(): Promise<void> {
    try {
      if((this.Swatch_DISTRICT !='0')&& (this.Swatch_MMC_CODE !='0') && (this.Swatch_GP_CODE !='0') && (this.Swatch_Ur =='R'))
      {
      const req = {
        type: '104',
        input01:this.Swatch_Ur.toString(),
        input02:this.Swatch_DISTRICT.toString(),
        input03:this.Swatch_MMC_CODE.toString(),
        input04:this.Swatch_GP_CODE.toString()
      };

      this.SurveyReportdata=[];  
        this.Swatch_VW_CODE = '0';
      
      this.villagearry = []; this.spinner.show();

      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));

      this.spinner.hide();

      this.villagearry = rsdata.Details;
    }

    } catch (error) {

    }
  } 
  SurveyReportdata: any[] = [];typeid:any='';mandalgpcode:any='';
  async  GetSurveyReportdata(): Promise<void> 
  {
    debugger;
    this.isgetdata=true;
    if((this.Swatch_DISTRICT !='0')&& (this.Swatch_MMC_CODE !='0') && (this.Swatch_GP_CODE !='0')&& (this.Swatch_Ur !=''))
    {
      if((this.Swatch_Ur =='U'))
      { this.typeid= this.roleid=='1004' ?'122' :'124';
          this.Swatch_VW_CODE=this.Swatch_GP_CODE ;
          this.mandalgpcode =this.Swatch_MMC_CODE;
      }
      else if((this.Swatch_Ur =='R'))
      {
        this.typeid=this.roleid=='1004' ? '123' :'125';;
        this.mandalgpcode =this.Swatch_GP_CODE;
      
      }
      this.isgetdata=false;
      const req = {
        type: this.typeid,
        input01:this.Swatch_VW_CODE == 'All' ? '' : this.Swatch_VW_CODE.toString(),
        input02:this.mandalgpcode 
      };
     this.allrecords=false;
     this.SurveyReportdata=[];   
     this.checkedrecords=[];
     this.finalcheckrecords=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      if(rsdata.code)
      {
        this.SurveyReportdata=rsdata.Details; 
        this.SurveyReportdata.forEach(function(e:any){
          if (typeof e === "object" ){
            e["SURVEY_CheckId"] = false
          }
        });
       
      }
      else{
        this.alt.toasterror('No data found')
      }
     
    }
  }


  generalinfoReportdata: any[] = [];
  async  Getgeneralinformationreportdata(Surveyid:any): Promise<void> 
  {
  debugger;
    if((this.Swatch_DISTRICT !='0')&& (this.Swatch_MMC_CODE !='0') && (this.Swatch_GP_CODE !='0'))
    {
     
      const req = {
        type: '106',
        input01:Surveyid.toString()
      };
this.generalinfoReportdata=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      debugger;
      this.generalinfoReportdata=rsdata.Details  ;
    }
  }

familyinfodata:any []=[];
  async  GetFamilymembersReportdata(Surveyid:any): Promise<void> 
  {
   
    if((this.Swatch_DISTRICT !='0')&& (this.Swatch_MMC_CODE !='0') && (this.Swatch_GP_CODE !='0'))
    {
    
      const req = {
        type: '107',
        input02: Surveyid.toString()
      };
this.familyinfodata=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      debugger;
      this.familyinfodata=rsdata.Details;
    }
  }


  Jobroledata:any []=[];
  async  GetjobroleReportdata(Surveyid:any): Promise<void> 
  {
    debugger;
    if((this.Swatch_DISTRICT !='0')&& (this.Swatch_MMC_CODE !='0') && (this.Swatch_GP_CODE !='0'))
    {
     
      const req = {
        type: '108',
        input08:Surveyid.toString()
      };
this.Jobroledata=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      this.Jobroledata=rsdata.Details;
    }
  }
  SocioReportdata:any[]=[];
  async  GetSocioReportdata(Surveyid:any): Promise<void> 
  {
    
    if((this.Swatch_DISTRICT !='0')&& (this.Swatch_MMC_CODE !='0') && (this.Swatch_GP_CODE !='0'))
    {
    debugger;
      const req = {
        type: '109',
        input06:Surveyid.toString()
      };
this.SocioReportdata=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      this.SocioReportdata=rsdata.Details;
    }
  }

  Insurencedata:any []=[];
  async  GetInsurenceReportdata(Surveyid:any): Promise<void> 
  {
    debugger;
    if((this.Swatch_DISTRICT !='0')&& (this.Swatch_MMC_CODE !='0') && (this.Swatch_GP_CODE !='0'))
    {
     
      const req = {
        type: '110',
        input05:Surveyid.toString()
      };
this.Insurencedata=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      this.Insurencedata=rsdata.Details;
    }
  }


  feedbackdata:any []=[];
  async  Getfeedbackdata(Surveyid:any): Promise<void> 
  {
    debugger;
    if((this.Swatch_DISTRICT !='0')&& (this.Swatch_MMC_CODE !='0') && (this.Swatch_GP_CODE !='0'))
    {
     
      const req = {
        type: '111',
        input10:Surveyid.toString()
      };
this.feedbackdata=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      this.feedbackdata=rsdata.Details;
    }
  }

  

  async ViewReports(Surveyid: string): Promise<void> {
    debugger;
    if((this.SurveyReportdata.length>0 ))
    {
    this.addModaldisplay='block'
   this.Getgeneralinformationreportdata(Surveyid);
   this.GetFamilymembersReportdata(Surveyid);
 this.GetSocioReportdata(Surveyid);
  this.GetInsurenceReportdata(Surveyid); 
  this.GetjobroleReportdata(Surveyid);
  this.Getfeedbackdata(Surveyid);
    }
  }
  exportSlipXl() {
    const Data:any= this.SurveyReportdata;
if(this.SurveyReportdata.length > 0)
{
    const checkdata = Math.max(Data.length);
    if(checkdata>0){
      debugger;
      //firstreport

      var body = [];
      var dataRow :any []=[];
      //Headers; 
      body.push(['','','','','Survey Report Data','','','',''],
      );
      body.push(dataRow);
      body.push(['S.no','Survey ID',
      'Surveyer Name'],);
      //body
      for (let i = 0; i < Data.length; i++) {
        var dataRow = [];
        dataRow = [i+1,Data[i].SURVEY_ID,
        Data[i].NAME];
        body.push(dataRow);
      }
      //footer
      body.push(['No Of Records:','','','','','','','',Data.length],
      );


      const Data1:any= this.generalinfoReportdata;
      const checkdata1 = Math.max(Data1.length);
      if(checkdata1>0){
      
      //var body = [];
      var dataRow :any []=[];
      //Headers; 
      body.push(['','','','','General Information','','','','',''],
      );
      body.push(dataRow);
      body.push(['S.No',
      'Survey ID',
      'Name','Father Name',
      'Gender',
      'Date Of Birth',
      'Age',
      'Marital Status','Caste',
      'Mobile',
      'Alternate Mobile',
      'Image',],);
      //body
      for (let i = 0; i < Data1.length; i++) {
        var dataRow = [];
        dataRow = [i+1,Data1[i].SURVEY_ID,Data1[i].NAME,
        Data1[i].FATHER_NAME,Data1[i].GENDER,Data1[i].DATE_OF_BIRTH,Data1[i].AGE,Data1[i].MARRIED,Data1[i].CASTE,Data1[i]. MOBILE,Data1[i].ALTER_MOBILE,Data1[i].IMAGE_PATH];
        body.push(dataRow);
      }
      //footer
      body.push(['No Of Records:','','','','','','','','','',Data1.length],
      );
    }

    const Data2:any= this.familyinfodata;
    const checkdata2 = Math.max(Data2.length);
    if(checkdata2>0){
     // var body = [];
      var dataRow :any []=[];
      //Headers; 
      body.push(['','','','','','','','','Family Details','','','','','','','',''],
      );
      body.push(dataRow);
      body.push(['S.No',
      'Survey ID',
      'Have Family',
      'Family Count',
      'Family Members Name',
      'Relation','Date Of Birth',
      'Age',
      'Gender',
      'Marital Status',
      'Category Name',
      'Educational Status Name',
      'Address',
      'Residence on Survey',
      'Educational Status Name',
      'Previous Generations Occupation',
      'How Many Generations Count', 
      'Previous Generations Name'],);
      //body
      for (let i = 0; i < Data2.length; i++) {
        var dataRow = [];
        dataRow = [i+1,Data2[i].SURVEY_ID,
        Data2[i].HAVE_FAMILY,Data2[i].FAMILY_MEMBERS_COUNT,Data2[i].FAMILY_MEMBERS_NAME,Data2[i].RELATION,Data2[i].DATE_OF_BIRTH,Data2[i].AGE,
        ,Data2[i].GENDER,Data2[i].MARRIED,Data2[i].CATEGORY_NAME,Data2[i].EDUCATIONAL_STATUS_NAME,Data2[i].ADDRESS,Data2[i].AT_RESIDENCE_ON_SURVEY,Data2[i].EDUCATIONAL_STATUS_NAME,Data2[i].PREVIOUS_GENERATIONS_INSAME_OCCUPATIONS,Data2[i].MANY_GENERATIONS_COUNT,Data2[i].PREVIOUS_GENERATIONS_NAME];
        body.push(dataRow);
      }
      //footer
      body.push(['No Of Records:','','','','','','','',Data2.length],
      );
    }

    const Data5:any= this.Jobroledata;
    const checkdata5 = Math.max(Data5.length);
    if(checkdata5>0){
      //var body = [];
      var dataRow :any []=[];
      //Headers; 
      body.push(['','','','','Job Role Information','','','',''],
      );
      body.push(dataRow);
      body.push(['S.No',
      'Survey Id',
      'Activity Text',
      'Activity Name',
      'Type Of Employment',
      'House For Work',
      'Being Paid Full Salary',
      'Being Treated',
      'Provided with ID card',
      'Card Name',
      'ID card in Current Location',
      'Monthly Working Days',
      'Monthly Income',
      'Basic Needs',
      'Salary Delayed',
      'Pay Slips',
      'Satisfied With Pay Slips',
      'Pay Slips Image Path',
      'ID card Path',
      ],);
      //body
      for (let i = 0; i < Data5.length; i++) {
        var dataRow = [];
        let ETN:string ='';
        if(Data5[i].UNAUTHORIZED_TEXT != null || Data5[i].UNAUTHORIZED_TEXT != '' )
        {
               ETN = Data5[i].EMPLOYMENT_TYPE_NAME + '('+Data5[i].UNAUTHORIZED_TEXT +')';
        }
        else{
          ETN = Data5[i].EMPLOYMENT_TYPE_NAME;
        }
        dataRow = [i+1,Data5[i].SURVEY_ID,
        Data5[i].ACTIVITIES_TEXT,Data5[i].ACTIVITIES_NAME,ETN.toString(),Data5[i].HOUSE_FOR_WORK,Data5[i].BEING_PAID_FULL_SALARY,Data5[i].BEING_TREATED,
        Data5[i].PROVIDED_WITH_IDCARD,Data5[i].CARD_NAME,
        Data5[i].IDCARD_IN_CURRENT_LOCATION,Data5[i].WORKING_DAYS_IN_MONTH,Data5[i].MONTHLY_INCOME_AMOUNT,Data5[i].PROVIDED_WITH_BASI_NEEDS,Data5[i].SALARY_DELAYED,
        Data5[i].PROVIDED_WITH_PAYSLIPS,Data5[i].SATISFIED_WITH_PAYSLIPS_PROVIDED,Data5[i].PAYSLIPS_IMAGEGPATH,Data5[i].PROVIDED_IDCARD_IMAGEPATH,];
        body.push(dataRow);
      }
      //footer
      body.push(['No Of Records:','','','','','','','','','','','','','','','',Data5.length],
      );
    }

const Data3:any= this.SocioReportdata;
const checkdata3 = Math.max(Data3.length);
if(checkdata3>0){
  //var body = [];
  var dataRow :any []=[];
  //Headers; 
  body.push(['','','','','Economic Information','','','',''],
  );
  body.push(dataRow);
  body.push(['S.No',
  'Survey Id',
  'Sanitation Work Name',
  'Type of House',
  'House Location',
  'Loan Type',
  'Rate of Interest',
  'Repay Loan',
  'Alternate Livehood',
  'Alternate Livehood Name',
  'Safety Trainer Name',
  'Training Hours'],);
 
  //body
  for (let i = 0; i < Data3.length; i++) {
    var dataRow = [];
    dataRow = [i+1,Data3[i].SURVEY_ID,Data3[i].SANITATION_WORK_NAME,
    Data3[i].TYPE_OF_HOUSE_NAME,Data3[i].HOUSE_LOCATION,Data3[i].LOAN_TYPE,Data3[i].RATE_OF_INTEREST,Data3[i].RE_PAY_LOAN,
    Data3[i].ALTERNATE_LIVELIHOODS,Data3[i].LIVELIHOOD_NAME,Data3[i].SAFETY_TRAINING_NAME,
    Data3[i].TRAINING_HR_NAME,];
    body.push(dataRow);
  }
  //footer
  body.push(['No Of Records:','','','','','','','','','','','','','','','','','',Data3.length],
  );
}

    const Data4:any= this.Insurencedata;
    const checkdata4 = Math.max(Data4.length);
    if(checkdata4>0){
     // var body = [];
      var dataRow :any []=[];
      //Headers; 
      body.push(['','','','','','Insurance Details','','','',''],
      );
      body.push(dataRow);
      body.push(['S.no','Survey ID',
      'Current Health Issue  Name',
      'Current Health Issue Sub Name',
      'Previous Health Issue  Name',
      'Previous Health Issue Sub  Name',
      'Death Sanitation Work Name',
      'Sanitation Work Death',
      'Insurance Name',
      'Health Checkup',
      'Other Health Checkup'
    ],);
  
  
   
      //body
      for (let i = 0; i < Data4.length; i++) {
        var dataRow = [];
        dataRow = [i+1,Data4[i].SURVEY_ID,
        Data4[i].DEATH_SANITATION_WORK_NAME,
        Data4[i].SANITAION_WORK_DEATH_OTHER,
        Data4[i].PREVIOUS_HEALTH_ISSUE_NAME,
        Data4[i].PREVIOUS_HEALTH_ISSUE_SUB_NAME,
        Data4[i].CURRENT_HEALTH_ISSUE_NAME,
        Data4[i].CURRENT_HEALTH_ISSUE_SUB_NAME,
        Data4[i].INSURANCE_NAME,
        Data4[i].HEALTH_CHECKUP_DONE,
        Data4[i].HEALTH_CHECKUP_OTHER,];
        body.push(dataRow);
      }
      //footer
      body.push(['No Of Records:','','','','','','','','',Data4.length],
      );
    }


    const Data6:any= this.feedbackdata;
    const checkdata6 = Math.max(Data6.length);
    if(checkdata6>0){
    
      var dataRow :any []=[];
     
      body.push(['','','Feedback Details','',''],
      );
      body.push(dataRow);
      body.push(['S.no','Survey ID',
      'Add Any Questions In Survey',
      'Remove Any Questions In survey','Feedback and Suggestions'],);
   
      for (let i = 0; i < Data6.length; i++) {
        var dataRow = [];
        dataRow = [i+1,Data6[i].SURVEY_ID,
        Data6[i].ADD_ANY_QUESTIONS_INSURVEY,Data6[i].REMOVE_ANY_QUESTIONS_INSURVEY,Data6[i].FEEDBACK_AND_SUGGESTIONS];
        body.push(dataRow);
      }
      //footer
      body.push(['No Of Records:','','','',Data6.length],
      );
    }
    this.mid.JSONToCSVConvertor(
      body,
      'Survey Report Data',
      true
     );
    }
  }
  }
  addModaldisplay:any='none';
  modalclose()
  {
    this.addModaldisplay = 'none';
  }

  img:any
  async ViewCatimg(imageurl: string): Promise<void> {
    debugger;
    this.Imagedisplay='block'
   if(this.mid.Deploystage =="UAT")
   {
    this.img = "https://apsanitationworkerssurvey.ap.gov.in//SWUAT//SwachhAndhraSurveyImages/"+imageurl;
   }
   else  if(this.mid.Deploystage =="PROD")
   {
    this.img = "https://apsanitationworkerssurvey.ap.gov.in//SWAPI//SwachhAndhraSurveyImages/"+imageurl;
   }
  
  
  }
  allrecords:any=false;
  Recordcheckall(event:any)
  {
    this.checkedrecords=[];
let arr_2 = this.SurveyReportdata.map(e=>Object.assign({},e));


debugger;

for(let elm of arr_2){ Reflect.deleteProperty(elm, 'SURVEY_CheckId') }
if(event.target.checked == true)
{this.allrecords=true;
  this.SurveyReportdata.forEach(function(e:any){
    if (typeof e === "object" ){
      e["SURVEY_CheckId"] = true
    }
  });
  var result = this.SurveyReportdata.map(function(val) {
    return val.SURVEY_ID;
  }).join(',');
  this.finalcheckrecords =result;
 
}
else if (event.target.checked == false)
{this.allrecords=false;
  this.SurveyReportdata.forEach(function(e:any){
    if (typeof e === "object" ){
      e["SURVEY_CheckId"] = false
    }
  });
  this.finalcheckrecords =[];
}
debugger;
  }

  checkedrecords:any[]=[];finalcheckrecords:any=[];
  Recordcheck(event:any,row: any) {
   debugger;
  if(this.allrecords == true)
  {
    if(this.checkedrecords.length==0)
    this.checkedrecords=this.SurveyReportdata;
  }
   if(event.target.checked== true)
   {
   let item ={ "SURVEY_ID":row.SURVEY_ID};

   this.checkedrecords.push(item);
   var result = this.checkedrecords.map(function(val) {
        return val.SURVEY_ID;
      }).join(',');

   this.finalcheckrecords =result;
  }
   else if(event.target.checked== false)
   {
    debugger;
    this.checkedrecords = this.checkedrecords.filter(recolist => recolist.SURVEY_ID !== row.SURVEY_ID);

     var result = this.checkedrecords.map(function(val) {
        return val.SURVEY_ID;
     }).join(',');
    this.finalcheckrecords = result;
   }
   debugger;
  }

  Imagedisplay='none';allrecords1:any=false;
  imgmodalclose()
  {
    this.Imagedisplay='none';
  }

 
  async GetApprovedata(Status:any): Promise<void> {
  
  if(this.finalcheckrecords.length > 0)
  {
      debugger;
      this.typeid=sessionStorage.getItem('userrole') =='1004'? '120':'121';
      const req = {
        type: this.typeid,
        input01:Status.toString(),
        input02:sessionStorage.getItem('username'),
        input03:this.finalcheckrecords,
      };
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      if(rsdata.code)
      {
       
       if(Status == "1")
       {
        this.alt.success('Approved Successfully' );
       }
       else  if(Status == "2")
       {
        this.alt.warning('Rejected Successfully');
       }
       this.GetSurveyReportdata();
      }
      else{
        this.alt.toasterror('No data found');
      }
  }
  else{
    this.alt.toasterror('Please select records');
  }
  }
}
