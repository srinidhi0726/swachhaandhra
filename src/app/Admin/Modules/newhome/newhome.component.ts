import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import type {
  FireworksDirective,
  FireworksOptions
} from '@fireworks-js/angular'

@Component({
  selector: 'app-newhome',
  templateUrl: './newhome.component.html',
  styleUrls: ['./newhome.component.css']
})
export class NewhomeComponent {

  enabled = true
  options: FireworksOptions = {
    opacity: 0.5
  }

  @ViewChild('fireworks') fireworks?: FireworksDirective

  toggleFireworks(): void {
    this.enabled = !this.enabled
  }

  waitStop(): void {
    this.fireworks?.waitStop()
  }

  
  constructor(
     
    private router: Router
 
  ) { 
  }
  Login()
  {
   this.router.navigate(["/login"]);
  // this.getverticalchart();
  }


  ngOnInit(): void {
     
  }

  

}
