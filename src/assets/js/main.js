(function ($) {
    "use strict";

    // Spinner
    var spinner = function () {
        setTimeout(function () {
            if ($('#spinner').length > 0) {
                $('#spinner').removeClass('show');
            }
        }, 1);
    };
    spinner();


    // Initiate the wowjs
    new WOW().init();


    // Dropdown on mouse hover
    const $dropdown = $(".dropdown");
    const $dropdownToggle = $(".dropdown-toggle");
    const $dropdownMenu = $(".dropdown-menu");
    const showClass = "show";

    $(window).on("load resize", function () {
        if (this.matchMedia("(min-width: 992px)").matches) {
            $dropdown.hover(
                function () {
                    const $this = $(this);
                    $this.addClass(showClass);
                    $this.find($dropdownToggle).attr("aria-expanded", "true");
                    $this.find($dropdownMenu).addClass(showClass);
                },
                function () {
                    const $this = $(this);
                    $this.removeClass(showClass);
                    $this.find($dropdownToggle).attr("aria-expanded", "false");
                    $this.find($dropdownMenu).removeClass(showClass);
                }
            );
        } else {
            $dropdown.off("mouseenter mouseleave");
        }
    });


    // Back to top button
    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('.back-to-top').fadeIn('slow');
        } else {
            $('.back-to-top').fadeOut('slow');
        }
    });
    $('.back-to-top').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 1500, 'easeInOutExpo');
        return false;
    });


    // Facts counter
    $('[data-toggle="counter-up"]').counterUp({
        delay: 10,
        time: 2000
    });


    // Modal Video
    $(document).ready(function () {
        var $videoSrc;
        $('.btn-play').click(function () {
            $videoSrc = $(this).data("src");
        });
        console.log($videoSrc);

        $('#videoModal').on('shown.bs.modal', function (e) {
            $("#video").attr('src', $videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0");
        })

        $('#videoModal').on('hide.bs.modal', function (e) {
            $("#video").attr('src', $videoSrc);
        })
    });


    // Testimonials carousel
    $(".testimonial-carousel").owlCarousel({
        autoplay: true,
        smartSpeed: 1000,
        margin: 25,
        dots: false,
        loop: true,
        nav: true,
        navText: [
            '<i class="fa fa-arrow-left"></i>',
            '<i class="fa fa-arrow-right"></i>'
        ],
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            }
        }
    });

// News Carousel
$(".news-carousel").owlCarousel({
    autoplay: true,
    smartSpeed: 1000,
    margin: 25,
    dots: false,
    loop: true,
    nav: true,
    navText: [
        '<i class="fa fa-arrow-left"></i>',
        '<i class="fa fa-arrow-right"></i>'
    ],
    responsive: {
        0: {
            items: 1
        },
        768: {
            items: 2
        }
    }
});

    // client carousel
    $(".client-carousel").owlCarousel({
        autoplay: true,
        smartSpeed: 500,
        margin: 25,
        dots: false,
        loop: true,
        nav: false,
        navText: [
            '<i class="fa fa-arrow-left"></i>',
            '<i class="fa fa-arrow-right"></i>'
        ],
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 6
            }
        }
    });

    // Navbar Fixed
    $(window).scroll(function () {
               if ($(window).scrollTop() > 63) {
          $('.navbar').addClass('navbar-fixed');
        }
        if ($(window).scrollTop() < 64) {
          $('.navbar').removeClass('navbar-fixed');
        }
     
      });

    // about carousel
    $(".about-carousel").owlCarousel({
        autoplay: true,
        smartSpeed: 1000,
        margin: 25,
        dots: false,
        loop: true,
        nav: false,
        navText: [
            '<i class="fa fa-arrow-left"></i>',
            '<i class="fa fa-arrow-right"></i>'
        ],
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            }
        }
    });

    const filterItem = document.querySelector(".items");
    const filterImg = document.querySelectorAll(".gallery .image");

    $(document).ready(function(){
        //$('.navbar').removeClass('navbar-fixed');
        const filterItem_SELECTED = document.querySelector(".items .item.active");
        const filterImg_selected = document.querySelectorAll(".gallery .image");

       // let filterName = filterImg_selected.target.getAttribute("data-name");

       console.log(filterImg_selected);
        
        filterImg_selected.forEach((image) => {
            let filterImges = image.getAttribute("data-name"); //getting image data-name value
             
            console.log(filterImges);
            if(filterImges=="fw"){

                  image.classList.add("show");
            }
            else{
                image.classList.add("hide");
                
            }
            
        });

    })

    window.onload = () => { //after window loaded

   

        filterItem.onclick = (selectedItem) => { //if user click on filterItem div
            if (selectedItem.target.classList.contains("item")) { //if user selected item has .item class
                filterItem.querySelector(".active").classList.remove("active"); //remove the active class which is in first item
                selectedItem.target.classList.add("active"); //add that active class on user selected item
                let filterName = selectedItem.target.getAttribute("data-name"); //getting data-name value of user selected item and store in a filtername variable
                filterImg.forEach((image) => {
                    let filterImges = image.getAttribute("data-name"); //getting image data-name value
                    //if user selected item data-name value is equal to images data-name value
                    //or user selected item data-name value is equal to "all"
                    if ((filterImges == filterName) || (filterName == "fm")) {
                        image.classList.remove("hide"); //first remove the hide class from the image
                        image.classList.add("show"); //add show class in image
                    } else {
                        image.classList.add("hide"); //add hide class in image
                        image.classList.remove("show"); //remove show class from the image
                    }
                });
            }
        }
        for (let i = 0; i < filterImg.length; i++) {
            filterImg[i].setAttribute("onclick", "preview(this)"); //adding onclick attribute in all available images
        }
    }
    

})(jQuery);